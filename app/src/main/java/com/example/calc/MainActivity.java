package com.example.calc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    EditText text;
    TextView textSum;
    Button btnCalc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.text);
        btnCalc = findViewById(R.id.btnCalc);
        textSum = findViewById(R.id.textSum);

        btnCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text.toString().contains("*")) {
                    String[] txt = text.toString().split("\\*");
                    int a = Integer.parseInt(txt[0]);
                    int b = Integer.parseInt(txt[1]);
                    int sum = a * b;
                    textSum.setText("result: " + sum);
                }
                else if (text.toString().contains("+")) {
                    String[] txt = text.toString().split("\\+");
                    int a = Integer.parseInt(txt[0]);
                    int b = Integer.parseInt(txt[1]);
                    int sum = a + b;
                    textSum.setText("result: " + sum);
                }
                else if (text.toString().contains("-")) {
                    String[] txt = text.toString().split("\\-");
                    int a = Integer.parseInt(txt[0]);
                    int b = Integer.parseInt(txt[1]);
                    int sum = a - b;
                    textSum.setText("result: " + sum);
                }
                else if (text.toString().contains(":")) {
                    String[] txt = text.toString().split("\\:");
                    int a = Integer.parseInt(txt[0]);
                    int b = Integer.parseInt(txt[1]);
                    if (b != 0) {
                        int sum = a / b;
                        textSum.setText("result: " + sum);
                    }
                    else textSum.setText("Делить на ноль нельзя");
                }
                else textSum.setText("Заполните поле");
            }
        });

    }
}
